<?php 

class Agent
{
	private $AgentId;
	private $AgtFirstName;
	private $AgtMiddleInitial;
	private $AgtLastName;
	private $AgtBusPhone;
	private $AgtEmail;
	private $AgtPosition;
	private $AgencyId;



	public function __construct($array)
	{
		$this->AgentId = $array["AgentId"];
		$this->AgtFirstName = $array["AgtFirstName"];
		$this->AgtMiddleInitial = (isset($array["AgtMiddleInitial"]) ? $array["AgtMiddleInitial"] : "");
		$this->AgtLastName = $array["AgtLastName"];
		$this->AgtBusPhone = $array["AgtBusPhone"];
		$this->AgtEmail = $array["AgtEmail"];
		$this->AgtPosition = $array["AgtPosition"];
		$this->AgencyId = $array["AgencyId"];
	}
	public function getAgentId()
 	{
 		return $this->AgentId;
 	}
 	public function setAgentId($id)
 	{
 		$this->AgentId=$id;
 	}
 	public function getAgtFirstName()
 	{
 		return $this->AgtFirstName;
 	}
 	public function setAgtFirstName($fname)
 	{
 		$this->AgentId=$fname;
 	}
 	public function getAgtMiddleInitial()
 	{
 		return $this->AgtMiddleInitial;
 	}
 	public function setAgtMiddleInitial($mname)
 	{
 		$this->AgtMiddleInitial=$mname;
 	}
 	public function getAgtLastName()
 	{
 		return $this->AgtLastName;
 	}
 	public function setAgtLastName($lname)
 	{
 		$this->AgtLastName=$lname;
 	}
 	public function getAgtBusPhone()
 	{
 		return $this->AgtBusPhone;
 	}
 	public function setAgtBusPhone($phone)
 	{
 		$this->AgtBusPhone=$phone;
 	}
 	public function getAgtEmail()
 	{
 		return $this->AgtEmail;
 	}
 	public function setAgtEmail($email)
 	{
 		$this->AgtEmail=$email;
 	}
 	public function getAgtPosition()
 	{
 		return $this->AgtPosition;
 	}
 	public function setAgtPosition($posion)
 	{
 		$this->AgtPosition=$posion;
 	}
 	public function getAgencyId()
 	{
 		return $this->AgencyId;
 	}
 	public function setAgencyId($agencyid)
 	{
 		$this->AgencyId=$agencyid;
 	}
 	public function toString()
 	{
 		  return $string =$this->AgtFirstName. ",".$this->AgtMiddleInitial. ","	.$this->AgtLastName. "," .$this->AgtBusPhone. "," .$this->AgtEmail. "," .$this->AgtPosition.",".$this->AgencyId;
 		
 	}

}

?>