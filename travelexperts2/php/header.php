<!DOCTYPE html>
<html>
<head>
	<title>Travel Experts - DEMO</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="img/mylogo.png" />
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/styles.css">
<!-- Varibles and functions for the travel package list section --> 
	<script type="text/javascript">
		<?php include 'php/urls.php';?>
      var win;
      var view = <?php echo json_encode($view); ?>;
      var detail= <?php echo json_encode($detail); ?>;
      var urls = <?php echo json_encode(array_keys($urls));?>;
      var PkgName = <?php echo json_encode($PkgName); ?>;
      var PkgDesc = <?php echo json_encode($PkgDesc); ?>;

      function showimage(index)
      {
          document.getElementById("imagedisplay").src = view[index];
          document.getElementById("imagedisplay").setAttribute("onClick", "openURL("+index+");");
          document.getElementById("packname").innerHTML = PkgName[index];
          document.getElementById("packdesc").innerHTML = PkgDesc[index];
         }
         function openURL(index)
         {
          win = window.open(urls[index]);
          setTimeout(function(){win.close()},3000);
         }
	</script>
</head>
