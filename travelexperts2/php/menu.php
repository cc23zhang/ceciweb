
<body>
  <!-- Navbar (sit on top) -->
<div class="w3-top">
  <div class="w3-bar" id="myNavbar">
    <a class="w3-bar-item w3-button w3-hover-black w3-hide-medium w3-hide-large w3-right" href="javascript:void(0);" onclick="toggleFunction()" title="Toggle Navigation Menu">
      <i class="fa fa-bars"></i>
    </a>
    <img class="w3-bar-item" id="logo" src="img/mylogo.png">
    <a href="#home" class="w3-bar-item w3-button">HOME</a>
    <?php 
    if(!isset($_SESSION["loggedin"]) or $_SESSION["loggedin"] == false) 
    {
      print("<a href='php/login.php' class='w3-bar-item w3-button w3-hide-small w3-right'>LOGIN</a>"); 
    }
    else
    {
      print("<a href='php/signout.php' class='w3-bar-item w3-button w3-hide-small w3-right'>SIGNOUT</a>");
      print("<a href='php/addagentform.php' class='w3-bar-item w3-button w3-hide-small w3-right'>ADD AGENT</a>");
    }

      ?>
    <a href="php/register.php" class="w3-bar-item w3-button w3-hide-small w3-right">REGISTER</a>
    <a href="#contact" class="w3-bar-item w3-button w3-hide-small w3-right">CONTACT</a>    
    <a href="#packs" class="w3-bar-item w3-button w3-hide-small w3-right">PACKAGES</a>
    
    

  </div>

  <!-- Navbar on small screens -->
  <div id="navDemo" class="w3-bar-block w3-white w3-hide w3-hide-large w3-hide-medium">
    <a href="#packs" class="w3-bar-item w3-button" onclick="toggleFunction()">PACKAGES</a>
    <a href="#contact" class="w3-bar-item w3-button" onclick="toggleFunction()">CONTACT</a>
    <a href="php/register.php" class="w3-bar-item w3-button" onclick="toggleFunction()">REGISTER</a>
    
    <?php 
    if(!isset($_SESSION["loggedin"]) or $_SESSION["loggedin"] == false) 
    {
      print("<a href='php/login.php' class='w3-bar-item w3-button' onclick='toggleFunction()'>LOGIN</a>"); 
    }
    else
    {
      print("<a href='php/signout.php' class='w3-bar-item w3-button' onclick='toggleFunction()'>SIGNOUT</a>");
      print("<a href='php/addagentform.php' class='w3-bar-item w3-button' onclick='toggleFunction()'>ADD AGENT</a>");
    }

    ?>
    
    
    
  </div>
</div>
