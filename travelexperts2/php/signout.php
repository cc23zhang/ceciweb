<?php
	session_start();
	if(!isset($_SESSION["loggedin"]))
	{
		header("Location:login.php");
		$_SESSION["message"]= "You must login first";
		 // print("You must login first");
	}
	else 
	{
		unset($_SESSION["loggedin"]);
		header("Location:../index.php?signedout");
	}
?>