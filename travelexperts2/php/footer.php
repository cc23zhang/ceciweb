<!-- Footer -->

<footer class="w3-center w3-black w3-padding-16">

<?php include "php/links.php" ?>


  <div class="w3-xlarge w3-section">
    <a href="http://www.facebook.com" target="_blank"><i class="fa fa-facebook-official w3-hover-opacity"></i></a>
    <a href="http://www.instagram.com" target="_blank"><i class="fa fa-instagram w3-hover-opacity"></i></a>
    <a href="https://www.snapchat.com" target="_blank"><i class="fa fa-snapchat w3-hover-opacity"></i></a>
    <a href="https://www.pinterest.ca" target="_blank"><i class="fa fa-pinterest-p w3-hover-opacity"></i></a>
    <a href="https://twitter.com" target="_blank"><i class="fa fa-twitter w3-hover-opacity"></i></a>
    <a href="https://www.linkedin.com" target="_blank"><i class="fa fa-linkedin w3-hover-opacity"></i></a>
  </div>
  <p>Copyright 2017 OOSD &copy; by Cecilia Zhang</p>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjadW8tuY2HF1HHvDXZRQwC5-uCwsVMwA"></script> 
<script src="js/googlemap.js"></script>
<script src="js/effects.js"></script>
</body>
</html>
