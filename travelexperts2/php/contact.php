<?php 
include "php/serverinfo.php";
$dbh = mysqli_connect($host,$user,$password,$dbname);
  if(!$dbh)
  {
    print(mysqli_connect_error(). "<br/>");
    exit();
  }
?>

<div class="w3-container w3-padding-32 w3-content" style="max-width:1100px"" id="contact">
  <h1>Contact Us</h1>
  <div class="w3-row w3-padding-32 w3-section">
    <div class="w3-col w3-half w3-container">      
      <!-- Add Google Maps -->
       <div id="map_div" class="w3-round-large w3-greyscale" style="width:100%;height:500px;"></div>       
    </div>

 <div id="agency" class="w3-col w3-quarter w3-panel w3-large">
  <!-- fetch agency records from database -->
  <?php 
  $sql= "SELECT AgncyAddress,AgncyCity,AgncyProv,AgncyPostal,AgncyCountry,AgncyPhone,AgncyFax FROM agencies";
  if($result = mysqli_query($dbh,$sql))
  {
    while($row = mysqli_fetch_row($result))
    {
    print("<h3 class='fa fa-map-marker fa-fw w3-margin-right'><b>$row[1]</b></h3>
        <h5>$row[0]<br/>$row[1], $row[2],$row[3], $row[4]</h5>
        <h5>Phone: $row[5]<br/>
          Fax: $row[6]</h5><p style='visibility:hidden'>devider<p>");
    } 
  }
?> 
  </div>
</div>

<div class="w3-row w3-padding-32 w3-section">
  <div class="w3-col w3-half w3-panel w3-large">
<!-- fetch Calgary agent records from database -->
    <h1>Calgary Office</h1>

<?php 
  $sql= "SELECT AgtFirstName,AgtMiddleInitial,AgtLastName,AgtBusPhone,AgtEmail,AgtPosition FROM agents WHERE AgencyId =1 ORDER BY ( CASE (AgtPosition) WHEN 'Senior Agent' THEN 1 WHEN 'Intermediate Agent' THEN 2 WHEN 'Junior Agent' THEN 3 END ) "; 
//agents in calgary agency
  if($result = mysqli_query($dbh,$sql))
  {
    while($row = mysqli_fetch_row($result))
    {
    print("<h3><b>$row[0] $row[1] $row[2]</b></h3> 
          <h5>$row[5]<br/>Phone:$row[3]<br/>
          Email:$row[4]</h5>");
    } 
  }
?>  
  </div>
  <div class="w3-col w3-half w3-panel w3-large">
    <h1>Okotoks Office</h1>

<!-- fetch Okotoks agent records from database -->
<?php 
  $sql= "SELECT AgtFirstName,AgtMiddleInitial,AgtLastName,AgtBusPhone,AgtEmail,AgtPosition FROM agents WHERE AgencyId =2 ORDER BY ( CASE (AgtPosition) WHEN 'Senior Agent' THEN 1 WHEN 'Intermediate Agent' THEN 2 WHEN 'Junior Agent' THEN 3 END ) "; 
//agents in Okotoks agency
  if($result = mysqli_query($dbh,$sql))
  {
    while($row = mysqli_fetch_row($result))
    {
    print("<h3><b>$row[0] $row[1] $row[2]</b></h3> 
          <h5>$row[5]<br/>Phone:$row[3]<br/>
          Email:$row[4]</h5>");
    } 
  }
?>  
  </div>

</div>
</div>