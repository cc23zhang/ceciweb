<?php
	session_start();
	if (isset($_SESSION["message"]))
	{
		$message = $_SESSION["message"];
		unset($_SESSION["message"]);
	}
	
	include("serverinfo.php");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Add Agent</title>
	<link rel="stylesheet" type="text/css" href="../css/formstyle.css">
	<script>
		function validate(myform)
		{
			if (myform.AgtFirstName.value == "")
			{
				alert("First Name is required");
				myform.AgtFirstName.focus();
				return false;
			}
			if (myform.AgtLastName.value == "")
			{
				alert("Last Name is required");
				myform.AgtLastName.focus();
				return false;
			}
			if (myform.AgtBusPhone.value == "")
			{
				alert("Phone number is required");
				myform.AgtBusPhone.focus();
				return false;
			}
			var regex = /^(\w+\.)*\w+@(\w+\.)+([a-z]{2,6})$/i;
			if (myform.AgtEmail.value == "")
			{
				alert("Email is required");
				myform.AgtEmail.focus();
				return false;
			}
			else if (!regex.test(myform.AgtEmail.value))
			{
				alert("Email format is not valid");
				myform.AgtEmail.focus();
				return false;
			}
			if (myform.AgtPosition.value == "")
			{
				alert("Position is required");
				myform.AgtPosition.focus();
				return false;
			}
			if (myform.AgencyId.value == "")
			{
				alert("Need to select an agency");
				myform.AgencyId.focus();
				return false;
			}
			return confirm("Continue submitting?");
		}
	</script>
</head>
<body>
	<p><?php (isset($message)) ? print($message) : print("") ; ?></p>

	<form method="get" action="addagent.php">
			<h1>-Add Agent Form-</h1>
			<div class="break"></div>
		Agent First Name:
		<input type="text" required="required" name="AgtFirstName" class="input" placeholder="First Name"/>
		<div class="break"></div>
		Agent Middle Initial:
		<input type="text" name="AgtMiddleInitial" class="input" placeholder="Middle Initial"/>
		<div class="break"></div>
		Agent Last Name:
		<input type="text" required="required" name="AgtLastName" class="input" placeholder="Last Name" />
		<div class="break"></div>
		Phone:
		<input type="text" required="required" name="AgtBusPhone" class="input" placeholder="Phone"/>
		<div class="break"></div>
		Email:
		<input type="text" required="required" name="AgtEmail" class="input" placeholder="Email"/><div class="break"></div>
		Position:
		<input type="text" required="required" name="AgtPosition" class="input" placeholder="Position"/>
		<div class="break"></div>
		Agency:
		<div class="select_join"><select name="AgencyId">
			<option value="">Select an agency</option>
			<?php
				$dbh = mysqli_connect($host, $user, $password, $dbname);
				if (!$dbh)
				{
					print(mysqli_connect_error() . "<br />");
					exit();
				}
				$result = mysqli_query($dbh, "SELECT AgencyId, AgncyAddress, AgncyCity FROM Agencies");
				while ($row = mysqli_fetch_row($result))
				{
					print("<option value='$row[0]'>$row[1], $row[2]</option>");
				}
				mysqli_close($dbh);
			?>
		</select></div>
		<div class="break"></div>
		<div class="clearfix">
     		<button type="submit" class="submitbtn"  onclick="return validate(this.form)">Save</button>
      		<a href="../index.php"><button type="button" class="cancelbtn">Back to Home Page</button></a>
    	</div>
	</form>
</body>
</html>


