<?php
	session_start();
	include "functions.php";

	function validate($data)
	{
		$message="";
		if($data["AgtFirstName"] == "")
		{
			$message .="Agent First Name must have a value<br/>";
			return $message;
		}
		if($data["AgtLastName"] == "")
		{
			$message .="Agent Last Name must have a value<br/>";
			return $message;
		}
		if($data["AgtBusPhone"] == "")
		{
			$message .="Phone must have a value<br/>";
			return $message;
		}
		if($data["AgtEmail"] == "")
		{
			$message .="Email Name must have a value<br/>";
			return $message;
		}
		if($data["AgtPosition"] == "")
		{
			$message .="Position must have a value<br/>";
			return $message;
		}
		if($data["AgencyId"] == "")
		{
			$message .="Agency must be selected<br/>";
			return $message;
		}
		return $message;
	}


	if($_REQUEST)
	{
		if ($message = validate($_REQUEST))
		{
			//store message in session to send back to form
			$_SESSION["message"] = $message;
			header("Location: addagentform.php");
		}
		else
		{
			$data = $_REQUEST;
		}
	}
	else
	{
		//store message in session to send back to form
		$_SESSION["message"] = "You must fill in the form first";
		header("Location: addagentform.php");
	}

	
?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<?php
		$message = insertRow("agents", $data);
		$_SESSION["message"] = $message . "Do you want to enter another one?";
		header("Location: addagentform.php");
	?>
</body>
</html>