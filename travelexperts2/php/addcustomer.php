<?php
	session_start();

	include "functions.php";
	function validate($data)
	{
		if($data["Username"] == "")
		{
			$message .="Username must have a value<br/>";
			return $message;
		}
		if($data["Password"] == "")
		{
			$message .="Password must have a value<br/>";
			return $message;
		}
		if($data["CustFirstName"] == "")
		{
			$message .="First Name must have a value<br/>";
			return $message;
		}
		if($data["CustLastName"] == "")
		{
			$message .="Last Name must have a value<br/>";
			return $message;
		}
		if($data["CustAddress"] == "")
		{
			$message .="Address must have a value<br/>";
			return $message;
		}
		if($data["CustCity"] == "")
		{
			$message .="City must have a value<br/>";
			return $message;
		}
		if($data["CustProv"] == "")
		{
			$message .="Province must have a value<br/>";
			return $message;
		}
		if($data["CustPostal"] == "")
		{
			$message .="Postal must have a value<br/>";
			return $message;
		}
		if($data["CustCountry"] == "")
		{
			$message .="Country must have a value<br/>";
			return $message;
		}
		if(($data["CustHomePhone"] == "") && ($data["CustBusPhone"] == ""))
		{
			$message .="Phone must have a value<br/>";
			return $message;
		}
		if($data["CustEmail"] == "")
		{
			$message .="Email Name must have a value<br/>";
			return $message;
		}
		if($data["AgentId"] == "")
		{
			$message .="Agent must be selected<br/>";
			return $message;
		}
		return $message;
	}


	if($_REQUEST)
	{
		if ($message = validate($_REQUEST))
		{
			//store message in session to send back to form
			$_SESSION["message"] = $message;
			echo $message;
			header("Location: register.php");
		}
		else
		{
			$data = $_REQUEST;
			$data["Password"] =  password_hash($data["Password"], PASSWORD_DEFAULT);
		}
	}
	else
	{
		//store message in session to send back to form
		$_SESSION["message"] = "You must submit the form first";
		echo $message;
		header("Location: register.php");
	}

	
?>


<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<?php
		$message = insertRow("customers", $data);
		$_SESSION["message"] = $message;
		if(!isset($_SESSION["success"]) or $_SESSION["success"]== false)
		{
			header("Location: register.php");
		}
		else
		{
			$_SESSION["message"] = "Register Successfully!";
			header("Location: ../index.php");
		}
		
		
	?>
</body>
</html>