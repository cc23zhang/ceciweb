<?php
	session_start();
	if (isset($_SESSION["message"]))
	{
		$message = $_SESSION["message"];
		unset($_SESSION["message"]);
	}
	
	include("serverinfo.php");
?>

<!DOCTYPE html>
<html>
<head>
	<title>Register</title>
	<link rel="stylesheet" type="text/css" href="../css/formstyle.css">
	<script>

			function focusFunction(index,input)
			{
				document.getElementById(index).style.visibility = "visible";
				document.getElementById(input).style.backgroundColor = "pink";				
			}
			function blurFunction(index,input)
			{
				document.getElementById(index).style.visibility = "hidden";
				document.getElementById(input).style.backgroundColor = "white";
		
			}
			var validate = function(myform)
			{
				if(myform.Username.value == "")
				{
					alert("User name is required");
					myform.Username.focus();
					return false;
				}
				if(myform.Password.value == "")
				{
					alert("Password is required");
					myform.Password.focus();
					return false;
				}
				if(myform.CustFirstName.value == "")
				{
					alert("First name is required");
					myform.CustFirstName.focus();
					return false;
				}
				if(myform.CustLastName.value == "")
				{
					alert("Last name is required");
					myform.CustLastName.focus();
					return false;
				}				
				if(myform.CustAddress.value == "")
				{
					alert("Address is required");
					myform.CustAddress.focus();
					return false;
				}
				if(myform.CustCity.value == "")
				{
					alert("City is required");
					myform.CustCity.focus();
					return false;
				}
				if(myform.CustProv.value == "")
				{
					alert("Province is required");
					myform.CustProv.focus();
					return false;
				}
				if(myform.CustPostal.value == "")
				{
					alert("Postal code is required");
					myform.CustPostal.focus();
					return false;
				}
				else
				{
				 	var regexp = /^[a-vxy]\d[a-z]\s?\d[a-z]\d$|^\d{5}$|^\d{5} \d{4}$/i;
					if(!regexp.test(myform.CustPostal.value))
					{
						alert("Invalid Postal/Zip code format");
						myform.CustPostal.focus();
						return false;
					}
				}
				if(myform.CustCountry.value == "")
				{
					alert("Country is required");
					myform.CustCountry.focus();
					return false;
				}
				if((myform.CustHomePhone.value == "") && (myform.CustBusPhone.value == ""))
				{
					alert("Phone is required");
					myform.CustHomePhone.focus();
					return false;
				}

				
				if(myform.CustEmail.value == "")
				{
					alert("Email is required");
					myform.CustEmail.focus();
					return false;
				}
				else
				{
					var regexp =/^(\w+\.)*\w+@(\w+\.)+([a-z]{2,6})$/i;
					if(!regexp.test(myform.CustEmail.value))
					{
						alert("Invalid email pattern");
						myform.CustEmail.focus();
						return false;
					}
				}
				if(myform.AgentId.value == "")
				{
					alert("Please select an agent");
					myform.AgentId.focus();
					return false;
				}

				return confirm("Continue submitting?");
			}

</script>

<body>
<p><?php (isset($message)) ? print($message) : print(""); ?></p>
		<form method="post" action="addcustomer.php">
		<h1>-Create Your Account-</h1>
		<div class="break"></div>
				<label for="Username">User Name:</label><br />
				<input type="text" id="Username" name="Username" onfocus="focusFunction('m2','Username')" onblur="blurFunction('m2','Username')" />
				<span id="m2" style="visibility:hidden;"> Please enter your user name in this field. </span>
				<div class="break"></div>

				<label for="Password" >Password:</label><br />
				<input type="password" name="Password" id="Password"  onfocus="focusFunction('m3','Password')" onblur="blurFunction('m3','Password')" />
				<span id="m3" style="visibility:hidden;"> Please enter your passward in this field. </span>
				<div class="break"></div>

				<label for="CustFirstName">First Name:</label><br />
				<input type="text" id="CustFirstName" name="CustFirstName" onfocus="focusFunction('m0','CustFirstName')" onblur="blurFunction('m0','CustFirstName')">
				<span id="m0" style="visibility:hidden;"> Please enter your first name in this field. </span>
				<div class="break"></div>

				<label for="CustLastName">Last Name:</label><br />
				<input type="text" id="CustLastName" name="CustLastName" onfocus="focusFunction('m1','CustLastName')" onblur="blurFunction('m1','CustLastName')" />
				<span id="m1" style="visibility:hidden;"> Please enter your Last name in this field. </span>
				<div class="break"></div>	

				<label for="CustAddress">Address:</label><br />
				<input type="text" id="CustAddress" name="CustAddress" onfocus="focusFunction('m4','CustAddress')" onblur="blurFunction('m4','CustAddress')" />
				<span id="m4" style="visibility:hidden;"> Please enter your address in this field. </span>
				<div class="break"></div>

				<label for="CustCity">City:</label><br />
				<input type="text" id="CustCity" name="CustCity" onfocus="focusFunction('m5','CustCity')" onblur="blurFunction('m5','CustCity')" />
				<span id="m5" style="visibility:hidden;"> Please enter your city in this field. </span>
				<div class="break"></div>

				<label for="CustProv">Province:</label><br />
				<input type="text" id="CustProv" name="CustProv" onfocus="focusFunction('m6','CustProv')" onblur="blurFunction('m6','CustProv')" />
				<span id="m6" style="visibility:hidden;"> Please enter your province in this field. </span>
				<div class="break"></div>

				<label for="CustPostal">Postal/Zip Code:</label><br />
				<input type="text" id="CustPostal" name="CustPostal" onfocus="focusFunction('m7','CustPostal')" onblur="blurFunction('m7','CustPostal')" />
				<span id="m7" style="visibility:hidden;"> Please enter your postal/zip code in this field. <br/> Format: <i>X1X 1X1</i>or <i>NNNNN-NNNNNN</i></span>
				<div class="break"></div>

				<label for="CustCountry">Country:</label><br />
				<input type="text" id="CustCountry" name="CustCountry" onfocus="focusFunction('m8','CustCountry')" onblur="blurFunction('m8','CustCountry')" />
				<span id="m8" style="visibility:hidden;"> Please enter your country in this field. </span>
				<div class="break"></div>

				<label for="CustHomePhone">Home Phone:</label><br />
				<input type="text" id="CustHomePhone" name="CustHomePhone" onfocus="focusFunction('m9','CustHomePhone')" onblur="blurFunction('m9','CustHomePhone')" />
				<span id="m9" style="visibility:hidden;"> Please enter your home phone  in this field. </span>
				<div class="break"></div>

				<label for="CustBusPhone">Work Phone:</label><br />
				<input type="text" id="CustBusPhone" name="CustBusPhone" onfocus="focusFunction('m10','CustBusPhone')" onblur="blurFunction('m10','CustBusPhone')" />
				<span id="m10" style="visibility:hidden;"> Please enter your work phone in this field. </span>
				<div class="break"></div>

				<label for="CustEmail" >Email:</label><br />
				<input type="email" name="CustEmail" id="CustEmail" onfocus="focusFunction('m11','CustEmail')" onblur="blurFunction('m11','CustEmail')"/>
				<span id="m11" style="visibility:hidden;"> Please enter your email in this field. <br/> Format: <i>username@subdomain.domain</i> </span>
				<div class="break"></div>

				<div class="select_join">Agent:<select id="AgentId" name="AgentId" onfocus="focusFunction('m12','AgentId')" onblur="blurFunction('m12','AgentId')">
				<option value="">Select an agent</option>
			<?php
				$dbh = mysqli_connect($host, $user, $password, $dbname);
				if (!$dbh)
				{
					print(mysqli_connect_error() . "<br />");
					exit();
				}
				$result = mysqli_query($dbh, "SELECT AgentId, AgtFirstName, AgtMiddleInitial, AgtLastName FROM Agents");
				while ($row = mysqli_fetch_row($result))
				{
					print("<option value='$row[0]'>$row[1], $row[2]$row[3]</option>");
				}
				mysqli_close($dbh);
			?>
			</select></div>
			<span id="m12" style="visibility:hidden;"> Please select your agent in this field. </span>		
			<div class="break"></div>

			<div class="clearfix">
				<button type="submit" class="submitbtn"  onclick="return validate(this.form)">Submit</button>
				<button type="reset" class="cancelbtn"  onclick="return confirm('Do you really want to reset this form?')" id="resetreg" >Rest</button>
				<a href="../index.php"><button type="button" class="goback">Back</button></a>
			</div>
			
		</form>
</body>
</html>