<?php
	session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>LogIn</title>
	<link rel="stylesheet" type="text/css" href="../css/formstyle.css">
</head>
<body>
	<p>	<?php
		if (isset($_SESSION["message"]))
		{
			print($_SESSION["message"]);
			unset($_SESSION["message"]);
		}
	?></p>
	  <form method="post" action="uservalidate.php">
	  	Username:
     <input type="text" name="userid" class="input" placeholder="Username">
       <div class="break"></div>
        Password:
     <input type="password" name="pwd" class="input" placeholder="Password">
     <div class="clearfix">
      <button type="submit" class="submitbtn">Log In</button>
      <button type="button" class="cancelbtn" onclick="history.back(-1)">Cancel</button>

    </div>
  </form>


</body>
</html>