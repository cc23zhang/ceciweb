<?php
session_start();
if(isset($_SESSION["message"]))
{
$message = $_SESSION["message"];
unset($_SESSION["message"]);
}
else
{
	$message="";
}

include'php/header.php'; 
?>


<?php include'php/menu.php';?>


<!-- First Parallax Image with Logo Text -->
<div class="bgimg-1 w3-display-container w3-opacity-min" id="home">
  <div class="w3-display-middle" style="white-space:nowrap;">
      <span class="w3-center w3-padding-large w3-black w3-xlarge w3-wide w3-animate-opacity">Welcome <span class="w3-hide-small">to Travel Experts</span></span>
  </div>
  <div id="message" class="w3-center w3-wide w3-animate-opacity message"><?php echo($message) ?></div>
  <div class="banner w3-center w3-xlarge w3-wide w3-animate-opacity w3-hide-small"> <?php include 'php/banner.php';?></div>     
</div>
 

<!-- Travel package list section -->
<div class="packagelist" id="packs">
	<div>
			<script>
				for (var i = 0; i < detail.length; i++) 
				{
					document.write("<button class='tablink' id='details' onmouseover='showimage("+ i + ")'>"+ detail[i] + "</button>");
				}
			</script>
	</div>

	<div class="w3-cell-row" >
	  <div class="w3-container w3-cell imgs" style="width:75%">
	    <img id="imagedisplay"onclick="" src=<?php echo json_encode($view[0]); ?> >
	  </div>
	  <div class="w3-container w3-white w3-cell">
	   		<h3 id="packname"><?php $str = json_encode($PkgName[0]); echo trim($str, '"'); ?></h3>
		 	<p id="packdesc"><?php $str = json_encode($PkgDesc[0]); echo trim($str, '"'); ?></p>
	  </div>
	</div>
</div>
<div class="w3-bar" style="background-color: #555; height: 60px;"><p style="visibility: hidden;">devider</p></div>


<!-- Contact Section -->
<?php include'php/contact.php' ?>


  

<?php include'php/footer.php'; ?>

